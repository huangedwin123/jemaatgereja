package unpri.skripsi.jemaatgereja.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import unpri.skripsi.jemaatgereja.R;
import unpri.skripsi.jemaatgereja.api.APIService;
import unpri.skripsi.jemaatgereja.api.APIUrl;
import unpri.skripsi.jemaatgereja.helper.UserAdapter;
import unpri.skripsi.jemaatgereja.models.TotalUsers;
import unpri.skripsi.jemaatgereja.models.Users;

/**
 * Created by Belal on 14/04/17.
 */

public class HomeFragment extends Fragment {

    private RecyclerView recyclerViewUsers;
    private RecyclerView.Adapter adapter;
    private ImageView ic_clear;
    private final long DELAY = 1000; // milliseconds
    private EditText atv_places;
    private TextView tvTotalUsers;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_home, container, false);
    }

    private Timer timer=new Timer();
    private String keyword="";
    public TextWatcher atvPlacesTextWatcher = new TextWatcher(){

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count,
                                      int after) {
        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            if(count>0){
                ic_clear.setVisibility(View.VISIBLE);
            }else {
                ic_clear.setVisibility(View.GONE);
            }
        }
        @Override
        public void afterTextChanged(final Editable input) {
            timer.cancel();
            timer = new Timer();
            timer.schedule(new TimerTask() {

                public void run() {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if(input.length()>1){
                                String lastCharacterLocation =input.toString().substring(input.length()-1,input.length());
                                if(lastCharacterLocation.equals(" ")){
                                    keyword = input.toString().substring(0, input.length()-1);
                                }
                                else {
                                    keyword = input.toString();
                                }

                            }
                            else if(input.length()==0){

                                keyword="";

                            }
                            getallUser(keyword);
                        }
                    });
                }
            }, DELAY);
        }
    };
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Home");

        recyclerViewUsers = (RecyclerView) view.findViewById(R.id.recyclerViewUsers);
        recyclerViewUsers.setHasFixedSize(true);
        recyclerViewUsers.setLayoutManager(new LinearLayoutManager(getActivity()));
        ic_clear = (ImageView) view.findViewById(R.id.ic_clear);
        atv_places = (EditText) view.findViewById(R.id.atv_places);
        tvTotalUsers = (TextView) view.findViewById(R.id.tvTotalUsers);
        atv_places.addTextChangedListener(atvPlacesTextWatcher);
        ic_clear.setOnClickListener(clear);
        getallUser("");
        getTotalUser();
    }

    private void getallUser(String keyword){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<Users> call;
        if(keyword.equals("")){
            call = service.getUsers();
        }else{
            call = service.getSpesificUsers(keyword);
        }
        call.enqueue(new Callback<Users>() {
            @Override
            public void onResponse(Call<Users> call, Response<Users> response) {
                adapter = new UserAdapter(response.body().getUsers(), getActivity());
                recyclerViewUsers.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<Users> call, Throwable t) {
            }
        });
    }
    private void getTotalUser(){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        Call<TotalUsers> call;
        call = service.getTotalUsers();
        call.enqueue(new Callback<TotalUsers>() {
            @Override
            public void onResponse(Call<TotalUsers> call, Response<TotalUsers> response) {
                tvTotalUsers.setText("JUMLAH JEMAAT = "+response.body().getUsers()+" Orang");
            }

            @Override
            public void onFailure(Call<TotalUsers> call, Throwable t) {
            }
        });
    }
    public View.OnClickListener clear = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            atv_places.setText("");
            ic_clear.setVisibility(View.GONE);
        }
    };
}
