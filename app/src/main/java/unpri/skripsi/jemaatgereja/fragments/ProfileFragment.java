package unpri.skripsi.jemaatgereja.fragments;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import unpri.skripsi.jemaatgereja.R;
import unpri.skripsi.jemaatgereja.api.APIService;
import unpri.skripsi.jemaatgereja.api.APIUrl;
import unpri.skripsi.jemaatgereja.helper.SharedPrefManager;
import unpri.skripsi.jemaatgereja.models.Result;
import unpri.skripsi.jemaatgereja.models.User;

/**
 * Created by Belal on 14/04/17.
 */

public class ProfileFragment extends Fragment implements View.OnClickListener {

    private Button buttonUpdate;
    private EditText editTextName, editTextEmail, editTextPassword;
    private RadioGroup radioGender;
    private EditText editTextTempatLahir;
    private EditText editTextTanggalLahir;
    private EditText editTextNoHp;
    private EditText editTextPendidikan;
    private EditText editTextPekerjaan;
    private EditText editTextAlamat;
    private EditText editTextNamaIstri;
    private EditText editTextPendidikanIstri;
    private EditText editTextPekerjaanIstri;
    private EditText editTextNamaAnak;
    private EditText editTextJumlahAnak;
    private EditText editTextNoKTP;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_profile, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Profile");

        buttonUpdate = (Button) view.findViewById(R.id.buttonUpdate);

        editTextName = (EditText) view.findViewById(R.id.editTextName);
        editTextEmail = (EditText) view.findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) view.findViewById(R.id.editTextPassword);

        editTextTempatLahir= (EditText) view.findViewById(R.id.tempatlahir);
        editTextTanggalLahir= (EditText) view.findViewById(R.id.tgl_lahir);
        editTextNoHp= (EditText) view.findViewById(R.id.nohp);
        editTextPendidikan= (EditText) view.findViewById(R.id.pendidikan);
        editTextPekerjaan= (EditText) view.findViewById(R.id.pekerjaan);
        editTextAlamat= (EditText) view.findViewById(R.id.alamat);
        editTextNamaIstri= (EditText) view.findViewById(R.id.nama_istri);
        editTextPendidikanIstri= (EditText) view.findViewById(R.id.pendidikan_istri);
        editTextPekerjaanIstri= (EditText) view.findViewById(R.id.pekerjaan_istri);
        editTextNamaAnak= (EditText) view.findViewById(R.id.nama_anak);
        editTextJumlahAnak= (EditText) view.findViewById(R.id.jumlah_anak);
        radioGender = (RadioGroup) view.findViewById(R.id.radioGender);
        editTextNoKTP= (EditText) view.findViewById(R.id.editTextNoKTP);
        buttonUpdate.setOnClickListener(this);

        User user = SharedPrefManager.getInstance(getActivity()).getUser();

        editTextName.setText(user.getName());
        editTextEmail.setText(user.getEmail());
//        editTextPassword.setText(user.getPassword());

        if (user.getGender().equalsIgnoreCase("male")) {
            radioGender.check(R.id.radioMale);
        } else {
            radioGender.check(R.id.radioFemale);
        }
        editTextTempatLahir.setText(user.getTempat_lahir());
        editTextTanggalLahir.setText(user.getTanggal_lahir());
        editTextNoHp.setText(user.getNo_hp());
        editTextPendidikan.setText(user.getPendidikan());
        editTextPekerjaan.setText(user.getPekerjaan());
        editTextAlamat.setText(user.getAlamat());
        editTextNamaIstri.setText(user.getNama_istri());
        editTextPendidikanIstri.setText(user.getPendidikan_istri());
        editTextPekerjaanIstri.setText(user.getPekerjaan_istri());
        editTextNamaAnak.setText(user.getNama_anak());
        editTextJumlahAnak.setText(user.getJumlah_anak());
        editTextNoKTP.setText(user.getNo_ktp());
    }

    private void updateUser() {
        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.setMessage("Updating...");
        progressDialog.show();

        final RadioButton radioSex = (RadioButton) getActivity().findViewById(radioGender.getCheckedRadioButtonId());

        String name = editTextName.getText().toString().trim()+"";
        String email = editTextEmail.getText().toString().trim();
        String password = editTextPassword.getText().toString().trim();
        String gender = radioSex.getText().toString();
        String alamat = editTextAlamat.getText().toString().trim();
        String tempat_lahir= editTextTempatLahir.getText().toString().trim();
        String tanggal_lahir= editTextTanggalLahir.getText().toString().trim();
        String nohp = editTextNoHp.getText().toString().trim();
        String pendidikan= editTextPendidikan.getText().toString().trim();
        String pekerjaan = editTextPekerjaan.getText().toString().trim();
        String nama_istri = editTextNamaIstri.getText().toString().trim()+"";
        String pendidikan_istri = editTextPendidikanIstri.getText().toString().trim()+"";
        String pekerjaan_istri = editTextPekerjaanIstri.getText().toString().trim()+"";;
        String jumlah_anak = editTextJumlahAnak.getText().toString().trim()+"";;
        String nama_anak = editTextNamaAnak.getText().toString().trim()+"";;
        String no_ktp = editTextNoKTP.getText().toString().trim()+"";;
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);

        User user = new User(SharedPrefManager.getInstance(getActivity()).getUser().getId(), name, email, password, gender,
                alamat,tempat_lahir,tanggal_lahir,nohp,pendidikan,pekerjaan,nama_istri,pendidikan_istri,pekerjaan_istri,
                jumlah_anak,nama_anak,no_ktp);

        Call<Result> call = service.updateUser(user.getId(),userParams(
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                user.getGender(),
                user.getAlamat(),
                user.getTempat_lahir(),
                user.getTanggal_lahir(),
                user.getNo_hp(),
                user.getPendidikan(),
                user.getPekerjaan(),
                user.getNama_istri(),
                user.getPendidikan_istri(),
                user.getPekerjaan_istri(),
                user.getJumlah_anak(),
                user.getNama_anak(),
                user.getNo_ktp()
        ));
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                if (!response.body().getError()) {
                    SharedPrefManager.getInstance(getActivity()).userLogin(response.body().getUser());
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
    public Map<String, String> userParams(String name, String email, String password, String gender,
                                          String alamat, String tempat_lahir, String tanggal_lahir, String no_hp,
                                          String pendidikan, String pekerjaan, String nama_istri, String pendidikan_istri,
                                          String pekerjaan_istri, String jumlah_anak, String nama_anak, String no_ktp){

        HashMap<String, String> maps = new HashMap<>();

        maps.put("name", name);
        maps.put("email", email);
        maps.put("password", password);
        maps.put("gender", gender);
        maps.put("alamat", alamat);
        maps.put("tempat_lahir", tempat_lahir);
        maps.put("tanggal_lahir", tanggal_lahir);
        maps.put("no_hp", no_hp);
        maps.put("pendidikan", pendidikan);
        maps.put("pekerjaan", pekerjaan);
        maps.put("nama_istri", nama_istri);
        maps.put("pendidikan_istri", pendidikan_istri);
        maps.put("pekerjaan_istri", pekerjaan_istri);
        maps.put("jumlah_anak", jumlah_anak);
        maps.put("nama_anak", nama_anak);
        maps.put("no_ktp", no_ktp);
        return maps;
    }
    @Override
    public void onClick(View view) {
        if (view == buttonUpdate) {
            updateUser();
        }
    }
}
