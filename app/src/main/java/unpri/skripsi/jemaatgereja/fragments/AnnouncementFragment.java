package unpri.skripsi.jemaatgereja.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import unpri.skripsi.jemaatgereja.R;
import unpri.skripsi.jemaatgereja.api.APIService;
import unpri.skripsi.jemaatgereja.api.APIUrl;
import unpri.skripsi.jemaatgereja.helper.AnnouncementAdapter;
import unpri.skripsi.jemaatgereja.helper.MessageAdapter;
import unpri.skripsi.jemaatgereja.helper.SharedPrefManager;
import unpri.skripsi.jemaatgereja.models.AnnouncementResponse;
import unpri.skripsi.jemaatgereja.models.Messages;

/**
 * Created by Huang on 7/31/2017.
 */

public class AnnouncementFragment extends Fragment {

    private RecyclerView recyclerViewAnnouncement;
    private AnnouncementAdapter adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.announcement, container, false);
    }


    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("Messages");

        recyclerViewAnnouncement = (RecyclerView) view.findViewById(R.id.recyclerViewAnnouncement);
        recyclerViewAnnouncement.setHasFixedSize(true);
        recyclerViewAnnouncement.setLayoutManager(new LinearLayoutManager(getActivity()));

        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        APIService service = retrofit.create(APIService.class);


        Call<AnnouncementResponse> call = service.getAnnouncement();

        call.enqueue(new Callback<AnnouncementResponse>() {
            @Override
            public void onResponse(Call<AnnouncementResponse> call, Response<AnnouncementResponse> response) {
                adapter = new AnnouncementAdapter(response.body().getAnnouncement(), getActivity());
                recyclerViewAnnouncement.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<AnnouncementResponse> call, Throwable t) {
                Toast.makeText(getActivity(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }
}
