package unpri.skripsi.jemaatgereja.activities;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TableRow;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import unpri.skripsi.jemaatgereja.R;
import unpri.skripsi.jemaatgereja.api.APIService;
import unpri.skripsi.jemaatgereja.api.APIUrl;
import unpri.skripsi.jemaatgereja.api.RestClient;
import unpri.skripsi.jemaatgereja.dialog.AnakDialog;
import unpri.skripsi.jemaatgereja.helper.SharedPrefManager;
import unpri.skripsi.jemaatgereja.models.Anak;
import unpri.skripsi.jemaatgereja.models.Result;
import unpri.skripsi.jemaatgereja.models.User;

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener {

    private Button buttonSignUp;
    private Button tambahAnakbtn;
    private EditText editTextName, editTextEmail, editTextPassword;
    private RadioGroup radioGender;
    private EditText editTextTempatLahir;
    private EditText editTextTanggalLahir;
    private EditText editTextNoHp;
    private EditText editTextPendidikan;
    private EditText editTextPekerjaan;
    private EditText editTextAlamat;
    private EditText editTextNamaIstri;
    private EditText editTextPendidikanIstri;
    private EditText editTextPekerjaanIstri;
    private EditText editTextNamaAnak;
    private EditText editTextJumlahAnak;
    private EditText editTextNoKTP;
    private Calendar calendar;
    private ArrayList<Anak> anaks;
    private TableLayout tl;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        initToolbar();
        buttonSignUp = (Button) findViewById(R.id.buttonSignUp);

        editTextName = (EditText) findViewById(R.id.editTextName);
        editTextEmail = (EditText) findViewById(R.id.editTextEmail);
        editTextPassword = (EditText) findViewById(R.id.editTextPassword);

        editTextTempatLahir= (EditText) findViewById(R.id.tempatlahir);
        editTextTanggalLahir= (EditText) findViewById(R.id.tgl_lahir);
        editTextNoHp= (EditText) findViewById(R.id.nohp);
        editTextPendidikan= (EditText) findViewById(R.id.pendidikan);
        editTextPekerjaan= (EditText) findViewById(R.id.pekerjaan);
        editTextAlamat= (EditText) findViewById(R.id.alamat);
        editTextNamaIstri= (EditText) findViewById(R.id.nama_istri);
        editTextPendidikanIstri= (EditText) findViewById(R.id.pendidikan_istri);
        editTextPekerjaanIstri= (EditText) findViewById(R.id.pekerjaan_istri);
        editTextNamaAnak= (EditText) findViewById(R.id.nama_anak);
        editTextJumlahAnak= (EditText) findViewById(R.id.jumlah_anak);
        editTextNoKTP= (EditText) findViewById(R.id.editTextNoKTP);

        radioGender = (RadioGroup) findViewById(R.id.radioGender);
        anaks =new ArrayList<Anak>();
        tambahAnakbtn = (Button) findViewById(R.id.tambah_anak);
        tambahAnakbtn.setOnClickListener(this);
        tl = (TableLayout) findViewById(R.id.tl);
        float scale = getResources().getDisplayMetrics().density;
        int dpAsPixels = (int) (1*scale + 0.5f);
        tl.setPadding(dpAsPixels,0,0,dpAsPixels);
        tl.setBackgroundResource(R.drawable.table_shape);
        calendar = Calendar.getInstance();

        editTextTanggalLahir.setOnClickListener(this);
        buttonSignUp.setOnClickListener(this);
    }
    private void initToolbar(){
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if(toolbar!=null) {
            setSupportActionBar(toolbar);
            toolbar.setNavigationOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        }
        ActionBar actionBar = getSupportActionBar();
        if(actionBar!=null) {
            actionBar.setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setTitle("Register");
        }
    }
    private void userSignUp() {

        //defining a progress dialog to show while signing up
        final ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Signing Up...");
        progressDialog.show();

        //getting the user values
        final RadioButton radioSex = (RadioButton) findViewById(radioGender.getCheckedRadioButtonId());

        String name = editTextName.getText().toString().trim()+"";;
        String email = editTextEmail.getText().toString().trim()+"";;
        String password = editTextPassword.getText().toString().trim();
        String gender = radioSex.getText().toString();
        String alamat = editTextAlamat.getText().toString().trim();
        String tempat_lahir= editTextTempatLahir.getText().toString().trim();
        String tanggal_lahir= editTextTanggalLahir.getText().toString().trim();
        String nohp = editTextNoHp.getText().toString().trim();
        String pendidikan= editTextPendidikan.getText().toString().trim()+"";;
        String pekerjaan = editTextPekerjaan.getText().toString().trim()+"";;
        String nama_istri = editTextNamaIstri.getText().toString().trim()+"";;
        String pendidikan_istri = editTextPendidikanIstri.getText().toString().trim()+"";;
        String pekerjaan_istri = editTextPekerjaanIstri.getText().toString().trim()+"";;
        String jumlah_anak = editTextJumlahAnak.getText().toString().trim()+"";;
        String nama_anak = editTextNamaAnak.getText().toString().trim()+"";;
        String no_ktp = editTextNoKTP.getText().toString().trim()+"";;


        //building retrofit object
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(APIUrl.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();

        //Defining retrofit api service
        APIService service = retrofit.create(APIService.class);

        //Defining the user object as we need to pass it with the call
        User user = new User(name, email, password, gender, alamat, tempat_lahir, tanggal_lahir
        ,nohp, pendidikan, pekerjaan, nama_istri, pendidikan_istri, pekerjaan_istri,
                jumlah_anak, nama_anak,no_ktp);

        //defining the call
//        Call<Result> call = service.createUser(userParams(
//                user.getName(),
//                user.getEmail(),
//                user.getPassword(),
//                user.getGender(),
//                user.getAlamat(),
//                user.getTempat_lahir(),
//                user.getTanggal_lahir(),
//                user.getNo_hp(),
//                user.getPendidikan(),
//                user.getPekerjaan(),
//                user.getNama_istri(),
//                user.getPendidikan_istri(),
//                user.getPekerjaan_istri(),
//                user.getJumlah_anak(),
//                user.getNama_anak())
//        );
//
//
//        //calling the api
//        call.enqueue(new Callback<Result>() {
//            @Override
//            public void onResponse(Call<Result> call, Response<Result> response) {
//                //hiding progress dialog
//                progressDialog.dismiss();
//
//                //displaying the message from the response as toast
//                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
//
//                //if there is no error
//                if (!response.body().getError()) {
//                    //starting profile activity
//                    finish();
//                    SharedPrefManager.getInstance(getApplicationContext()).userLogin(response.body().getUser());
//                    startActivity(new Intent(getApplicationContext(), HomeActivity.class));
//                }
//            }
//
//            @Override
//            public void onFailure(Call<Result> call, Throwable t) {
//                progressDialog.dismiss();
//                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
//            }
//        });

        APIService apiService = RestClient.getClient();
        Call<Result> call = apiService.createUser(userParams(
                user.getName(),
                user.getEmail(),
                user.getPassword(),
                user.getGender(),
                user.getAlamat(),
                user.getTempat_lahir(),
                user.getTanggal_lahir(),
                user.getNo_hp(),
                user.getPendidikan(),
                user.getPekerjaan(),
                user.getNama_istri(),
                user.getPendidikan_istri(),
                user.getPekerjaan_istri(),
                user.getJumlah_anak(),
                user.getNama_anak(),
                user.getNo_ktp())
        );
        call.enqueue(new Callback<Result>() {
            @Override
            public void onResponse(Call<Result> call, Response<Result> response) {
                progressDialog.dismiss();

                //displaying the message from the response as toast
                Toast.makeText(getApplicationContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                if (response.isSuccessful()) {
                    // request successful (status code 200, 201)
                    if(response.body().getError()==false) {
                        finish();
                        SharedPrefManager.getInstance(getApplicationContext()).userLogin(response.body().getUser());
                        startActivity(new Intent(getApplicationContext(), HomeActivity.class));
                    }
                } else {
                }
            }

            @Override
            public void onFailure(Call<Result> call, Throwable t) {
                progressDialog.dismiss();
                Toast.makeText(getApplicationContext(), t.getMessage(), Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {
        if (view == buttonSignUp) {
            userSignUp();
        }
        else if(view == editTextTanggalLahir){

            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(),0);

            new DatePickerDialog(SignUpActivity.this,date,calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),
                    calendar.get(Calendar.DAY_OF_MONTH)).show();

            view.getParent().requestChildFocus(view,view);

        }
        else if(view == tambahAnakbtn){
            AnakDialog dialog=new AnakDialog(SignUpActivity.this);
            dialog.setOnDialogComplete(new AnakDialog.OnDialogComplete() {
                @Override
                public void OnDialogComplete(Anak value) {
                    anaks.add(value);
                    addRow(value);
                }
            });
            dialog.show();
            view.getParent().requestChildFocus(view,view);
        }
    }


    private void addHeader(){
        TableRow tableRow=new TableRow(SignUpActivity.this);
        tableRow.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT
        ));

        TextView no = new TextView(SignUpActivity.this);
        TextView tvNama = new TextView(SignUpActivity.this);
        TextView tvUsia = new TextView(SignUpActivity.this);
        TextView tvPekerjaan = new TextView(SignUpActivity.this);
        View view = new View(SignUpActivity.this);

        no.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,1));

        tvNama.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,2));

        tvUsia.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,1));

        tvPekerjaan.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,2));

        view.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,1));

        no.setGravity(Gravity.CENTER);
        tvNama.setGravity(Gravity.CENTER);
        tvUsia.setGravity(Gravity.CENTER);
        tvPekerjaan.setGravity(Gravity.CENTER);

        no.setBackgroundResource(R.drawable.cell_shape);
        tvNama.setBackgroundResource(R.drawable.cell_shape);
        tvUsia.setBackgroundResource(R.drawable.cell_shape);
        tvPekerjaan.setBackgroundResource(R.drawable.cell_shape);
        view.setBackgroundResource(R.drawable.cell_shape);

        no.setText("No");
        tvNama.setText("Nama");
        tvUsia.setText("Usia");
        tvPekerjaan.setText("Pekerjaan");

        tableRow.addView(no);
        tableRow.addView(tvNama);
        tableRow.addView(tvUsia);
        tableRow.addView(tvPekerjaan);
        tableRow.addView(view);

        tl.addView(tableRow);
        tl.setStretchAllColumns(true);
    }


    private void addRow(Anak anak){
        TableRow tableRow=new TableRow(SignUpActivity.this);
        tableRow.setLayoutParams(new TableRow.LayoutParams(
                TableRow.LayoutParams.WRAP_CONTENT,
                TableRow.LayoutParams.WRAP_CONTENT
        ));

        TextView no = new TextView(SignUpActivity.this);
        TextView tvNama = new TextView(SignUpActivity.this);
        TextView tvUsia = new TextView(SignUpActivity.this);
        TextView tvPekerjaan = new TextView(SignUpActivity.this);
        RelativeLayout relativeLayout = new RelativeLayout(SignUpActivity.this);
        Button btDelete = new Button(SignUpActivity.this);
        relativeLayout.addView(btDelete);


        no.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,1));

        tvNama.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,2));

        tvUsia.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,1));

        tvPekerjaan.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.MATCH_PARENT,2));

        relativeLayout.setLayoutParams(new TableRow.LayoutParams(
                0,
                TableRow.LayoutParams.WRAP_CONTENT,1));

        no.setGravity(Gravity.CENTER);
        tvNama.setGravity(Gravity.CENTER);
        tvUsia.setGravity(Gravity.CENTER);
        tvPekerjaan.setGravity(Gravity.CENTER);
        btDelete.setGravity(Gravity.CENTER);

        no.setBackgroundResource(R.drawable.cell_shape);
        tvNama.setBackgroundResource(R.drawable.cell_shape);
        tvUsia.setBackgroundResource(R.drawable.cell_shape);
        tvPekerjaan.setBackgroundResource(R.drawable.cell_shape);
        relativeLayout.setBackgroundResource(R.drawable.cell_shape);


        no.setText((tl.getChildCount() == 0 ? (tl.getChildCount()+1) : tl.getChildCount()) + "");


        tvNama.setText(anak.getNama());
        tvUsia.setText("" + anak.getUsia());
        tvPekerjaan.setText(anak.getPekerjaan());
        btDelete.setText("X");

        tableRow.addView(no);
        tableRow.addView(tvNama);
        tableRow.addView(tvUsia);
        tableRow.addView(tvPekerjaan);
        tableRow.addView(relativeLayout);

        btDelete.setOnClickListener(deleteRow);

        if(tl.getChildCount() == 0){
            addHeader();
        }

        tl.addView(tableRow);
        tl.setStretchAllColumns(true);
    }

    View.OnClickListener deleteRow=new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            if(tl.getChildCount()==2){
                tl.removeAllViews();
                anaks.clear();
            }
            else {
                View va = (View)v.getParent().getParent();
                int idx = tl.indexOfChild(va);
                tl.removeViewAt(idx);
                anaks.remove(idx-1);
            }
        }
    };

    private DatePickerDialog.OnDateSetListener date = new DatePickerDialog.OnDateSetListener(){
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            editTextTanggalLahir.setText(dayOfMonth + "-" + month + "-" + year);
        }
    };

    public Map<String, String> userParams(String name, String email, String password, String gender,
                                          String alamat, String tempat_lahir, String tanggal_lahir, String no_hp,
                                          String pendidikan, String pekerjaan, String nama_istri, String pendidikan_istri,
                                          String pekerjaan_istri, String jumlah_anak, String nama_anak,String no_ktp){

        HashMap<String, String> maps = new HashMap<>();
        maps.put("name", name);
        maps.put("email", email);
        maps.put("password", password);
        maps.put("gender", gender);
        maps.put("alamat", alamat);
        maps.put("tempat_lahir", tempat_lahir);
        maps.put("tanggal_lahir", tanggal_lahir);
        maps.put("no_hp", no_hp);
        maps.put("pendidikan", pendidikan);
        maps.put("pekerjaan", pekerjaan);
        maps.put("nama_istri", nama_istri);
        maps.put("pendidikan_istri", pendidikan_istri);
        maps.put("pekerjaan_istri", pekerjaan_istri);
        maps.put("jumlah_anak", jumlah_anak);
        maps.put("nama_anak", nama_anak);
        maps.put("no_ktp", no_ktp);
        return maps;
    }
}
