package unpri.skripsi.jemaatgereja;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.TextView;

/**
 * Created by Huang on 7/15/2017.
 */

public class Register extends AppCompatActivity{
    private TextView tvName;
    private TextView tvCallName;
    private TextView tvPassword;
    private Spinner spGender;
    private TextView tvBirthPlace;
    private TextView tvNohp;
    private TextView tvTanggalLahir;
    private TextView tvPendidikan;
    private TextView tvPekerjaan;
    private TextView tvStatus;
    private TextView tvAlamat;
    private Button btAddMoreMember;
    private Button btRegister      ;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.register);
//        initToolbar();
        renderViews();
    }
    private void renderViews(){
        tvName = (TextView) findViewById(R.id.nama);
        tvPassword= (TextView) findViewById(R.id.password);
        tvCallName = (TextView) findViewById(R.id.callname);

//        spGender= (Spinner) findViewById(R.id.gender);
        tvBirthPlace = (TextView) findViewById(R.id.tempatlahir);
        tvTanggalLahir= (TextView) findViewById(R.id.tgl_lahir);
        tvNohp = (TextView) findViewById(R.id.nohp);
        tvPendidikan = (TextView) findViewById(R.id.pendidikan);
        tvPekerjaan = (TextView) findViewById(R.id.pekerjaan);
        tvAlamat = (TextView) findViewById(R.id.alamat);
        tvStatus= (TextView) findViewById(R.id.status);
//        btAddMoreMember= (Button) findViewById(R.id.bt_add_more_member);
        btRegister= (Button) findViewById(R.id.bt_register);
    }

}
