package unpri.skripsi.jemaatgereja.api;

import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.http.Field;
import retrofit2.http.FieldMap;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import unpri.skripsi.jemaatgereja.models.Announcement;
import unpri.skripsi.jemaatgereja.models.AnnouncementResponse;
import unpri.skripsi.jemaatgereja.models.MessageResponse;
import unpri.skripsi.jemaatgereja.models.Messages;
import unpri.skripsi.jemaatgereja.models.Result;
import unpri.skripsi.jemaatgereja.models.TotalUsers;
import unpri.skripsi.jemaatgereja.models.Users;

/**
 * Created by Belal on 14/04/17.
 */

public interface APIService {

    @FormUrlEncoded
    @POST("register")
    Call<Result> createUser(@FieldMap Map<String, String> params);


    @FormUrlEncoded
    @POST("login")
    Call<Result> userLogin(
            @Field("no_ktp") String email,
                            @Field("password") String password
    );

    @GET("users")
    Call<Users> getUsers();
    @GET("users/{keyword}")
    Call<Users> getSpesificUsers(@Path("keyword") String user);

    @FormUrlEncoded
    @POST("sendmessage")
    Call<MessageResponse> sendMessage(
            @Field("from") int from,
            @Field("to") int to,
            @Field("title") String title,
            @Field("message") String message);

    @FormUrlEncoded
    @POST("update/{id}")
    Call<Result> updateUser(
            @Path("id") int id,
            @FieldMap Map<String, String> params
    );

    //getting messages
    @GET("messages/{id}")
    Call<Messages> getMessages(@Path("id") int id);

    //getting messages
    @GET("totaluser")
    Call<TotalUsers> getTotalUsers();

    @GET("announcement")
    Call<AnnouncementResponse> getAnnouncement();


}
