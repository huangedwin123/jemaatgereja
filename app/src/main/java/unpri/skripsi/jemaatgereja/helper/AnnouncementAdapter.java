package unpri.skripsi.jemaatgereja.helper;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import unpri.skripsi.jemaatgereja.R;
import unpri.skripsi.jemaatgereja.models.Announcement;
import unpri.skripsi.jemaatgereja.models.AnnouncementResponse;
import unpri.skripsi.jemaatgereja.models.Message;

/**
 * Created by Huang on 7/31/2017.
 */

public class AnnouncementAdapter extends RecyclerView.Adapter<AnnouncementAdapter.ViewHolder> {

    private List<Announcement> announcement;
    private Context mCtx;

    public AnnouncementAdapter(ArrayList<Announcement> announcement, Context mCtx) {
        this.announcement = announcement;
        this.mCtx = mCtx;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.announcement_item, parent, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        final Announcement announcement = this.announcement.get(position);
        holder.textViewFrom.setText(announcement.getFrom());
        holder.textViewTitle.setText(announcement.getTitle());
        holder.textViewContent.setText(announcement.getContent());
        holder.textViewTime.setText(announcement.getDate());
    }


    @Override
    public int getItemCount() {
        return announcement.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textViewFrom;
        public TextView textViewTitle;
        public TextView textViewContent;
        public TextView textViewTime;

        public ViewHolder(View itemView) {
            super(itemView);
            textViewFrom = (TextView) itemView.findViewById(R.id.from);
            textViewTitle = (TextView) itemView.findViewById(R.id.title);
            textViewContent = (TextView) itemView.findViewById(R.id.content);
            textViewTime = (TextView) itemView.findViewById(R.id.time);
        }
    }
}
