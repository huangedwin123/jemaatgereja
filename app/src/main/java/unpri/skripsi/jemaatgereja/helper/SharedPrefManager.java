package unpri.skripsi.jemaatgereja.helper;

import android.content.Context;
import android.content.SharedPreferences;

import unpri.skripsi.jemaatgereja.models.User;


/**
 * Created by Belal on 14/04/17.
 */

public class SharedPrefManager {

    private static SharedPrefManager mInstance;
    private static Context mCtx;

    private static final String SHARED_PREF_NAME = "simplifiedcodingsharedprefretrofit";

    private static final String KEY_USER_ID = "keyuserid";
    private static final String KEY_USER_NAME = "keyusername";
    private static final String KEY_USER_EMAIL = "keyuseremail";
    private static final String KEY_USER_GENDER = "keyusergender";
    private static final String KEY_USER_ALAMAT= "keyuseralamat";
    private static final String KEY_USER_TEMPAT_LAHIR= "keyusertempatlahir";
    private static final String KEY_USER_TANGGAL_LAHIR= "keyusertanggallahir";
    private static final String KEY_USER_NO_HP= "keyusernohp";
    private static final String KEY_USER_PENDIDIKAN= "keyuserpendidikan";
    private static final String KEY_USER_PEKERJAAN= "keyuserpekerjaan";
    private static final String KEY_USER_NAMA_ISTRI= "keyusernamaistri";
    private static final String KEY_USER_PENDIDIKAN_ISTRI= "keyuserpendidikanistri";
    private static final String KEY_USER_PEKERJAAN_ISTRI= "keyuserpekerjaanistri";
    private static final String KEY_USER_JUMLAH_ANAK= "keyuserjumlahanak";
    private static final String KEY_USER_NAMA_ANAK= "keyusernamaanak";
    private static final String KEY_USER_NO_KTP= "keyusernoktp";

    private SharedPrefManager(Context context) {
        mCtx = context;
    }

    public static synchronized SharedPrefManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SharedPrefManager(context);
        }
        return mInstance;
    }

    public boolean userLogin(User user) {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt(KEY_USER_ID, user.getId());
        editor.putString(KEY_USER_NAME, user.getName());
        editor.putString(KEY_USER_NO_KTP, user.getNo_ktp());
        editor.putString(KEY_USER_EMAIL, user.getEmail());
        editor.putString(KEY_USER_GENDER, user.getGender());
        editor.putString(KEY_USER_ALAMAT, user.getAlamat());
        editor.putString(KEY_USER_TEMPAT_LAHIR, user.getTempat_lahir());
        editor.putString(KEY_USER_TANGGAL_LAHIR, user.getTanggal_lahir());
        editor.putString(KEY_USER_NO_HP, user.getNo_hp());
        editor.putString(KEY_USER_PENDIDIKAN, user.getPendidikan());
        editor.putString(KEY_USER_PEKERJAAN, user.getPekerjaan());
        editor.putString(KEY_USER_NAMA_ISTRI, user.getNama_istri());
        editor.putString(KEY_USER_PENDIDIKAN_ISTRI, user.getPendidikan_istri());
        editor.putString(KEY_USER_PEKERJAAN_ISTRI, user.getPekerjaan_istri());
        editor.putString(KEY_USER_NAMA_ANAK, user.getNama_anak());
        editor.putString(KEY_USER_JUMLAH_ANAK, user.getJumlah_anak());
        editor.apply();
        return true;
    }

    public boolean isLoggedIn() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        if (sharedPreferences.getString(KEY_USER_NO_KTP, null) != null)
            return true;
        return false;
    }

    public User getUser() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        return new User(
                sharedPreferences.getInt(KEY_USER_ID, 0),
                sharedPreferences.getString(KEY_USER_NAME, null),
                sharedPreferences.getString(KEY_USER_EMAIL, null),
                sharedPreferences.getString(KEY_USER_GENDER, null),
                sharedPreferences.getString(KEY_USER_ALAMAT, null),
                sharedPreferences.getString(KEY_USER_TEMPAT_LAHIR, null),
                sharedPreferences.getString(KEY_USER_TANGGAL_LAHIR, null),
                sharedPreferences.getString(KEY_USER_NO_HP, null),
                sharedPreferences.getString(KEY_USER_PENDIDIKAN, null),
                sharedPreferences.getString(KEY_USER_PEKERJAAN, null),
                sharedPreferences.getString(KEY_USER_NAMA_ISTRI, null),
                sharedPreferences.getString(KEY_USER_PENDIDIKAN_ISTRI, null),
                sharedPreferences.getString(KEY_USER_PEKERJAAN_ISTRI, null),
                sharedPreferences.getString(KEY_USER_NAMA_ANAK, null),
                sharedPreferences.getString(KEY_USER_JUMLAH_ANAK, null),
                sharedPreferences.getString(KEY_USER_NO_KTP, null)
        );
    }

    public boolean logout() {
        SharedPreferences sharedPreferences = mCtx.getSharedPreferences(SHARED_PREF_NAME, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear();
        editor.apply();
        return true;
    }
}
