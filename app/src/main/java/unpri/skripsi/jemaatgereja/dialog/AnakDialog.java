package unpri.skripsi.jemaatgereja.dialog;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import unpri.skripsi.jemaatgereja.R;
import unpri.skripsi.jemaatgereja.models.Anak;

/**
 * Created by wiled on 9/3/17.
 */

public class AnakDialog extends Dialog implements View.OnClickListener{
    public Button tambah;
    public EditText nama,usia,pekerjaan;
    private OnDialogComplete onDialogComplete;
    private Anak anak;

    public AnakDialog(Context context){
        super(context);
    }

    public AnakDialog(Context context,Anak anak) {
        super(context);
        this.anak=anak;
    }

    private Boolean isNotNullOrEmpty(String str){
        return str != null && !str.trim().isEmpty();
    }

    public interface OnDialogComplete
    {
        public void OnDialogComplete(Anak value);
    }

    public void setOnDialogComplete (OnDialogComplete onDialogComplete)
    {
        // Store the listener object
        this.onDialogComplete = onDialogComplete;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dialog_anak);
        tambah  = (Button) findViewById(R.id.tambah);
        nama = (EditText) findViewById(R.id.nama_anak);
        usia = (EditText) findViewById(R.id.umur);
        pekerjaan = (EditText) findViewById(R.id.pekerjaan);
        tambah.setOnClickListener(this);

        if(anak != null){
            nama.setText(anak.getNama());
            usia.setText(anak.getUsia());
            pekerjaan.setText(anak.getPekerjaan());
        }

        WindowManager.LayoutParams params = getWindow().getAttributes();
        params.width = WindowManager.LayoutParams.MATCH_PARENT;
        getWindow().setAttributes((android.view.WindowManager.LayoutParams) params);
    }

    @Override
    public void onClick(View v) {
        if(v == tambah){
            if(isNotNullOrEmpty(nama.getText().toString()) &&
                    isNotNullOrEmpty(usia.getText().toString()) &&
                    isNotNullOrEmpty(pekerjaan.getText().toString())){

                onDialogComplete.OnDialogComplete(new Anak(nama.getText().toString(),
                        Integer.parseInt(usia.getText().toString()),
                        pekerjaan.getText().toString()));

                dismiss();
            }
            else{
                Toast.makeText(this.getContext(),"Harap cek kembali",Toast.LENGTH_SHORT).show();
            }
        }
    }

    @Override
    public void onBackPressed() {
        dismiss();
    }
}
