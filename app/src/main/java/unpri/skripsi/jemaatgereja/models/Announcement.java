package unpri.skripsi.jemaatgereja.models;

/**
 * Created by Huang on 7/31/2017.
 */

public class Announcement {
    private int id;
    private String from;
    private String title;
    private String content;
    private String date;

    public Announcement(int id, String from, String title, String content, String date) {
        this.id = id;
        this.from = from;
        this.title = title;
        this.content = content;
        this.date = date;
    }

    public int getId() {
        return id;
    }

    public String getFrom() {
        return from;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getDate() {
        return date;
    }
}
