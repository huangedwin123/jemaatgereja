package unpri.skripsi.jemaatgereja.models;

/**
 * Created by Belal on 14/04/17.
 */

public class User {

    private int id;
    private String name;
    private String email;
    private String password;
    private String gender;
    private String alamat;
    private String tempat_lahir;
    private String tanggal_lahir;
    private String no_hp;
    private String pendidikan;
    private String pekerjaan;
    private String nama_istri;
    private String pendidikan_istri;
    private String pekerjaan_istri;
    private String jumlah_anak;
    private String nama_anak;
    private String no_ktp;


    public User(String name, String email, String password, String gender) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.gender = alamat;
    }
    public User(int id, String name, String email, String gender,
                String alamat, String tempat_lahir, String tanggal_lahir, String no_hp, String pendidikan,
                String pekerjaan, String nama_istri, String pendidikan_istri,String pekerjaan_istri,
                String jumlah_anak, String nama_anak, String no_ktp) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.gender = gender;
        this.alamat = alamat;
        this.tempat_lahir = tempat_lahir;
        this.tanggal_lahir = tanggal_lahir;
        this.no_hp = no_hp;
        this.pendidikan= pendidikan;
        this.pekerjaan= pekerjaan;
        this.nama_istri = nama_istri;
        this.pendidikan_istri = pendidikan_istri;
        this.pekerjaan_istri = pekerjaan_istri;
        this.jumlah_anak = jumlah_anak;
        this.nama_anak = nama_anak;
        this.no_ktp = no_ktp;
    }
    public User(String name, String email,String password, String gender,
                String alamat, String tempat_lahir, String tanggal_lahir, String no_hp, String pendidikan,
                String pekerjaan, String nama_istri, String pendidikan_istri,String pekerjaan_istri,
                String jumlah_anak, String nama_anak, String no_ktp) {
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.alamat = alamat;
        this.tempat_lahir = tempat_lahir;
        this.tanggal_lahir = tanggal_lahir;
        this.no_hp = no_hp;
        this.pendidikan= pendidikan;
        this.pekerjaan= pekerjaan;
        this.nama_istri = nama_istri;
        this.pendidikan_istri = pendidikan_istri;
        this.pekerjaan_istri = pekerjaan_istri;
        this.jumlah_anak = jumlah_anak;
        this.nama_anak = nama_anak;
        this.no_ktp = no_ktp;
    }
    public User(int id, String name, String email, String password, String gender,
                String alamat, String tempat_lahir, String tanggal_lahir, String no_hp, String pendidikan,
                String pekerjaan, String nama_istri, String pendidikan_istri,String pekerjaan_istri,
                String jumlah_anak, String nama_anak, String no_ktp) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
        this.alamat = alamat;
        this.tempat_lahir = tempat_lahir;
        this.tanggal_lahir = tanggal_lahir;
        this.no_hp = no_hp;
        this.pendidikan= pendidikan;
        this.pekerjaan= pekerjaan;
        this.nama_istri = nama_istri;
        this.pendidikan_istri = pendidikan_istri;
        this.pekerjaan_istri = pekerjaan_istri;
        this.jumlah_anak = jumlah_anak;
        this.nama_anak = nama_anak;
        this.no_ktp = no_ktp;
    }

    public User(int id, String name, String email, String password, String gender) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.password = password;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword(){
        return password;
    }

    public String getGender() {
        return gender;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public String getAlamat() {
        return alamat;
    }

    public void setAlamat(String alamat) {
        this.alamat = alamat;
    }

    public String getTempat_lahir() {
        return tempat_lahir;
    }

    public void setTempat_lahir(String tempat_lahir) {
        this.tempat_lahir = tempat_lahir;
    }

    public String getTanggal_lahir() {
        return tanggal_lahir;
    }

    public void setTanggal_lahir(String tanggal_lahir) {
        this.tanggal_lahir = tanggal_lahir;
    }

    public String getNo_hp() {
        return no_hp;
    }

    public void setNo_hp(String no_hp) {
        this.no_hp = no_hp;
    }

    public String getPendidikan() {
        return pendidikan;
    }

    public void setPendidikan(String pendidikan) {
        this.pendidikan = pendidikan;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public String getNama_istri() {
        return nama_istri;
    }

    public void setNama_istri(String nama_istri) {
        this.nama_istri = nama_istri;
    }

    public String getPendidikan_istri() {
        return pendidikan_istri;
    }

    public void setPendidikan_istri(String pendidikan_istri) {
        this.pendidikan_istri = pendidikan_istri;
    }

    public String getPekerjaan_istri() {
        return pekerjaan_istri;
    }

    public void setPekerjaan_istri(String pekerjaan_istri) {
        this.pekerjaan_istri = pekerjaan_istri;
    }

    public String getJumlah_anak() {
        return jumlah_anak;
    }

    public void setJumlah_anak(String jumlah_anak) {
        this.jumlah_anak = jumlah_anak;
    }

    public String getNama_anak() {
        return nama_anak;
    }

    public void setNama_anak(String nama_anak) {
        this.nama_anak = nama_anak;
    }

    public String getNo_ktp() {
        return no_ktp;
    }

    public void setNo_ktp(String no_ktp) {
        this.no_ktp = no_ktp;
    }
}
