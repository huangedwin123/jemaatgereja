package unpri.skripsi.jemaatgereja.models;

/**
 * Created by wiled on 9/3/17.
 */

public class Anak {
    private String nama;
    private int usia;
    private String pekerjaan;

    public Anak(String nama,int usia,String pekerjaan){
        this.nama=nama;
        this.usia=usia;
        this.pekerjaan = pekerjaan;
    }

    public int getUsia() {
        return usia;
    }

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public void setPekerjaan(String pekerjaan) {
        this.pekerjaan = pekerjaan;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setUsia(int usia) {
        this.usia = usia;
    }
}
