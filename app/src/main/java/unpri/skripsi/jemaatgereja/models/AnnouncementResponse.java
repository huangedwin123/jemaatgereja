package unpri.skripsi.jemaatgereja.models;

import java.util.ArrayList;

/**
 * Created by Huang on 7/31/2017.
 */

public class AnnouncementResponse {
    private ArrayList<Announcement> announcement;

    public AnnouncementResponse() {

    }

    public ArrayList<Announcement> getAnnouncement() {
        return announcement;
    }

    public void setAnnouncement(ArrayList<Announcement> announcement) {
        this.announcement = announcement;
    }
}
